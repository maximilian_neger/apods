Rails.application.routes.draw do
  root to: redirect('/apods')

  resources :apods, constraints: { id: /\d{4}-\d{2}-\d{2}/ },
                    only: [:index, :show] do
    collection do
      get 'search'
    end
  end

  match '/404', to: 'errors#not_found', via: :all
  match '/400', to: 'errors#bad_request', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
end
