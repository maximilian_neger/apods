require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AstroPics
  class Application < Rails::Application
    config.eager_load_paths << "#{config.root}/lib"

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.exceptions_app = routes
    config.action_dispatch.rescue_responses.merge!(
      'ApodNotFoundError' => :not_found,
      'RateLimitReachedError' => :not_found,
      'ImpossibleApodError' => :bad_request
    )
  end
end
