# rubocop: disable all
# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160308074756) do

  create_table "apods", force: :cascade do |t|
    t.date     "date",        null: false
    t.string   "explanation", null: false
    t.string   "media_type",  null: false
    t.string   "url",         null: false
    t.string   "hdurl"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "title",       null: false
    t.string   "copyright"
  end

  add_index "apods", ["date"], name: "index_apods_on_date", unique: true

end
