class DatabaseConstraints < ActiveRecord::Migration
  def change
    change_column_null :apods, :date, false
    change_column_null :apods, :title, false
    change_column_null :apods, :explanation, false
    change_column_null :apods, :media_type, false
    change_column_null :apods, :url, false
    change_column_null :apods, :hdurl, false
  end
end
