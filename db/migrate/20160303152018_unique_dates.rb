class UniqueDates < ActiveRecord::Migration
  def change
    add_index :apods, :date, unique: true
  end
end
