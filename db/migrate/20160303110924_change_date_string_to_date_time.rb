class ChangeDateStringToDateTime < ActiveRecord::Migration
  def change
    change_column :apods, :date, :datetime
  end
end
