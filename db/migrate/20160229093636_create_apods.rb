class CreateApods < ActiveRecord::Migration
  def change
    create_table :apods do |t|
      t.string :date
      t.string :explanation
      t.string :media_type
      t.string :url
      t.string :hdurl

      t.timestamps null: false
    end
  end
end
