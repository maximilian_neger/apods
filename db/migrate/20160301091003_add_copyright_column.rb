class AddCopyrightColumn < ActiveRecord::Migration
  def change
    add_column :apods, :copyright, :string
  end
end
