class HdUrlsCanBeNull < ActiveRecord::Migration
  def change
    change_column_null :apods, :hdurl, true
  end
end
