# encoding: utf-8

require 'faraday'
require 'faraday_middleware'

class Apod < ActiveRecord::Base
  validates :title, presence: true, length: { maximum: 200 }
  validates :explanation, presence: true, length: { maximum: 5000 }
  validates :url, presence: true, length: { maximum: 200 }
  validates :hdurl, length: { maximum: 200 }
  validates :media_type, presence: true, length: { maximum: 100 }
  validates :date, presence: true, uniqueness: true
  validates :copyright, length: { maximum: 100 }

  def self.searched(search)
    where('LOWER(title) LIKE LOWER(?) OR LOWER(explanation) LIKE LOWER(?)',
          "%#{search}%", "%#{search}%")
  end

  def self.find_for_date(date)
    apod = Apod.find_by(date: date)
    unless apod
      apod = Apod.new(date: date)
      apod.fetch_data
      apod.save
    end
    apod
  end

  def previous
    earliest_apod = Date.new(1995, 06, 16)
    Apod.find_for_date(date.yesterday) if date.yesterday >= earliest_apod
  end

  def next
    Apod.find_for_date(date.tomorrow) if date.tomorrow <= Date.today
  end

  def fetch_data
    connection = Faraday.new build_url do |conn|
      conn.response :json, content_type: /\bjson$/
      conn.adapter Faraday.default_adapter
    end

    response = connection.get
    handle_possible_errors(response)

    data = response.body
    data.delete('service_version')

    self.attributes = data
  end

  def image?
    media_type == 'image'
  end

  def formatted_date
    date.to_s
  end

  def to_param
    formatted_date
  end

  private

  def handle_possible_errors(api_response)
    if rate_limit_reached?(api_response)
      raise RateLimitReachedError, 'Das RateLimit wurde erreicht. Versuchs '\
                                   'doch gleich nochmal'
    end

    code = api_response.body['code']

    if code
      if code == 400
        raise ImpossibleApodError, 'Dieses Datum liegt außerhalb der ' \
                                   'erlaubten Datumsgrenzen.'
      end

      if code > 400
        raise ApodNotFoundError, 'Dieser Apod konnte nicht geladen werden.'
      end
    end
  end

  def build_url
    api_key = Rails.application.secrets[:nasa_api_key]
    "https://api.nasa.gov/planetary/apod?api_key=#{api_key}" \
      "&date=#{formatted_date}&hd=true"
  end

  def rate_limit_reached?(api_response)
    remaining = api_response.headers['x-ratelimit-remaining'].to_i
    remaining == 0
  end
end
