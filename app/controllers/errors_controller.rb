class ErrorsController < ApplicationController
  before_action { @exception = env['action_dispatch.exception'] }

  [:not_found, :bad_request, :internal_server_error].each do |type|
    define_method type do
      respond_to do |format|
        format.html { render status: type }
        format.any { head type }
      end
    end
  end
end
