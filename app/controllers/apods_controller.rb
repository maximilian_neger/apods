class ApodsController < ApplicationController
  def index
    load_apod(Date.today)
  end

  def show
    load_apod(Date.parse(params[:id]))
  end

  def search
    @apods = Apod.searched(params[:search]).paginate(page: params[:page])
    @search_term = params[:search]
  end

  private

  def load_apod(date)
    @apod = Apod.find_for_date(date)
    @next_apod = @apod.try(:next)
    @previous_apod = @apod.try(:previous)
  end
end
