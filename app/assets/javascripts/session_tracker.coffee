HOUR = 1000 * 60 * 60

SessionTracker =
  init: ->
    apod_info = @load_info()
    now = Date.now()

    if (now - apod_info.last_view > HOUR) and
       (apod_info.last_viewed_id? isnt @_extract_current_view_id())
      @ask_to_resume(apod_info)

    @log_last_view(now, apod_info)

  load_info: (info_key = 'apod_info') ->
    info = JSON.parse(localStorage.getItem(info_key))
    unless info
      info = Object.create(null)
      info.last_view = Date.now()
      info.last_viewed_id = @_extract_current_view_id()
    info

  _extract_current_view_id: ->
    url = new URL(window.location)
    url.pathname.match(/\d{4}-\d{2}-\d{2}/)?.pop()

  log_last_view: (time, apod_info, key='apod_info') ->
    apod_info.last_view = time
    apod_info.last_viewed_id = @_extract_current_view_id()
    localStorage.setItem(key, JSON.stringify(apod_info))

  navigate_to: (model_id) ->
    url = new URL(window.location)
    url.pathname = "/apods/#{model_id}"
    window.location = url

  ask_to_resume: (apod_info) ->
    choice = confirm('Willkommen zurück! Möchtest du dort starten wo du beim
                      letzten Mal aufgehört hast?')
    if choice then @navigate_to(apod_info.last_viewed_id)

SessionTracker.init()
