KeyboardController =
  keys:
    H: 104
    L: 108
    SLASH: 47

  actions:
    click: (elem) ->
      elem.click()

    focus: (elem) ->
      elem.focus()

  handle_key_pressed: (event) ->
    return if event.target isnt document.body

    switch event.which
      when @keys.H then @actions.click(@dom.previous_button)
      when @keys.L then @actions.click(@dom.next_button)
      when @keys.SLASH then @actions.focus(@dom.search_input) if event.shiftKey

  init: (dom) ->
    @dom = dom
    $(window).on "keypress", @handle_key_pressed.bind(this)

$ ->
  KeyboardController.init
    next_button: document.getElementById('js-next-apod')
    previous_button: document.getElementById('js-previous-apod')
    search_input: document.getElementById('js-search-input')
