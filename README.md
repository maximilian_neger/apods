# APOD-Betrachter

Eine kleine Rails App, welche das Astro Picture of the Day (APOD) herunterlädt
und anzeigt.

### Features
- Vorwärts- und Rückwärtsnavigation zwischen APODs
- Einfachstes Durchsuchen von geladenen APODs
- Keyboard Shortcuts für Zurück (h), Vorwärts (l) und Suchen (/)
- Session-Wiederaufnahme nach 2 Stunde Abwesenheit
